ipe (7.2.26+dfsg1-3) unstable; urgency=medium

  [ Steve Robbins ]
  * [6fb6e64] Replace gsfonts by fonts-urw-base35.  Closes: #1020364.

 -- Steve M. Robbins <smr@debian.org>  Fri, 14 Oct 2022 01:22:20 -0500

ipe (7.2.26+dfsg1-2) unstable; urgency=medium

  [ Steve Robbins ] 
  * Useless revision to case source rebuild.

 -- Steve M. Robbins <smr@debian.org>  Tue, 19 Jul 2022 20:19:20 -0500

ipe (7.2.26+dfsg1-1) unstable; urgency=medium

  [ Steve Robbins ]
  * [4ae2c7c] New upstream version 7.2.26+dfsg1
  * [2cc6e46] & [76e5ea4] adjust lib package name to 7.2.26
  * [15479cd] & [0c9f06b] Remove pkg-config patch that was applied upstream.

 -- Steve M. Robbins <smr@debian.org>  Sat, 16 Jul 2022 12:59:23 -0500

ipe (7.2.25+dfsg1-1) unstable; urgency=medium

  [ Steve Robbins ]
  * [a006df7] New upstream version 7.2.25+dfsg1
  * [c338927] Add dep for libqtspell.
  * [1931f14] refresh patches
  * [e91e943] adjust lib package name to 7.2.25.

 -- Steve M. Robbins <smr@debian.org>  Tue, 21 Jun 2022 23:55:16 -0500

ipe (7.2.24+dfsg1-1) unstable; urgency=medium

  [ Steve Robbins ]
  * [f7c2ed9] New upstream version 7.2.24
  * [28b6c67] Update library package name to libipe7.2.24.

 -- Steve M. Robbins <smr@debian.org>  Sat, 11 Sep 2021 20:57:22 -0500

ipe (7.2.23+dfsg1-2) unstable; urgency=medium

  [ Steve Robbins ]
  * [acfeb5f] Make the docs target a dependency of build-arch 
    (not install) so that it will be picked up in --arch=any builds.  
    Closes: #979311

 -- Steve M. Robbins <smr@debian.org>  Sun, 10 Jan 2021 16:52:36 -0600

ipe (7.2.23+dfsg1-1) unstable; urgency=medium

  [ Steve Robbins ]
  * [5c9852f] Add watch file, update copyright to repack without build 
    directory.
  * [1172b5b] New upstream version 7.2.23
  * [36bad4b] Adjust build for 7.2.23.  Add build-deps doxygen and libspiro.
  * [ec5c5e4] Sources do not provide manual in editable form, so curent 
    Debian policy prohibits distributing a manual.
  * [f1c333c] Patch "open manual" action to load online version.

 -- Steve M. Robbins <smr@debian.org>  Sat, 26 Dec 2020 10:24:35 -0600

ipe (7.2.22-1) unstable; urgency=medium

  [ Steve Robbins ]
  * [985e924] New upstream version 7.2.22
  * [cf09add] rebased patches
  * [2900b4a] Adjust build for 7.2.22
  * [72e07e3] Update Standards version and copyright text (taken from website).

 -- Steve M. Robbins <smr@debian.org>  Sun, 20 Dec 2020 13:21:21 -0600

ipe (7.2.20-2) unstable; urgency=medium
  * Source upload.

 -- Steve M. Robbins <smr@debian.org>  Sat, 12 Sep 2020 21:23:02 -0500

ipe (7.2.20-1) unstable; urgency=medium

  [ Steve Robbins ]
  * [6260979] New upstream version 7.2.20
  * [b01fb06] Rebase patches

 -- Steve M. Robbins <smr@debian.org>  Sun, 23 Aug 2020 21:44:23 -0500

ipe (7.2.17-2) unstable; urgency=medium

  [ Steve Robbins ]
  * push to unstable.

 -- Steve M. Robbins <smr@debian.org>  Sun, 21 Jun 2020 11:49:39 -0500

ipe (7.2.17-1) experimental; urgency=medium

  [ Steve Robbins ]
  * [9875e10] New upstream version 7.2.17
  * [f6b17d4] Update version string in control & rules.

 -- Steve M. Robbins <smr@debian.org>  Sat, 09 May 2020 13:42:00 -0500

ipe (7.2.16-1) experimental; urgency=medium

  [ Steve Robbins ]
  * [de5c228] New upstream version 7.2.16
  * [15518a1] Rebase patches.

 -- Steve M. Robbins <smr@debian.org>  Sat, 02 May 2020 15:39:03 -0500

ipe (7.2.15-1) unstable; urgency=medium

  [ Steve Robbins ]
  * New upstream version 7.2.15
  * Rebase patches

 -- Steve M. Robbins <smr@debian.org>  Mon, 27 Apr 2020 00:51:26 -0500

ipe (7.2.13-2) unstable; urgency=medium

  [ Steve Robbins ]
  * Adhere to environment PKG_CONFIG setting, to enable cross-build.
    Closes: #888522. Thanks to Helmut Grohne for the patch.

 -- Steve M. Robbins <smr@debian.org>  Sun, 19 Jan 2020 13:23:26 -0600

ipe (7.2.13-1) unstable; urgency=medium

  [ Steve Robbins ]
  * New upstream version 7.2.13.  Closes: #942780.

 -- Steve M. Robbins <smr@debian.org>  Fri, 17 Jan 2020 12:48:55 -0600

ipe (7.2.9-1) unstable; urgency=medium

  [ Steven Robbins ]
  * New upstream.
  * [b1a5fd4] Update packaging for IPE version 7.2.9.
  * [9bd4957] ipe depends on sensible-utils

 -- Steve M. Robbins <smr@debian.org>  Sun, 27 Jan 2019 23:43:18 -0600

ipe (7.2.7-3) unstable; urgency=medium

  * Team upload.
  * Moved packaging from SVN to Git
  * cme fix dpkg-control
  * debhelper 11
  * Standards-Version: 4.1.3
  * Fix build issue with glibc >= 2.26
    Closes: #887693
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Thu, 25 Jan 2018 14:23:21 +0100

ipe (7.2.7-2) unstable; urgency=medium

  * Team upload.
  * patches/wheel-zoom-debug.patch: New patch; removes unwanted noisy debug
    output. Closes: #851303.

 -- James Clarke <jrtc27@debian.org>  Wed, 18 Jan 2017 14:18:03 +0000

ipe (7.2.7-1) unstable; urgency=medium

  * New upstream.
  * Remove obsolete debian/ipe.png.uu.

 -- Steve M. Robbins <smr@debian.org>  Tue, 27 Dec 2016 21:42:30 -0600

ipe (7.2.6-1) unstable; urgency=medium

  * New upstream.
  * patches/default-browser.patch: Drop.  Now uses xdg-open for prefs.

 -- Steve M. Robbins <smr@debian.org>  Mon, 10 Oct 2016 04:00:55 -0500

ipe (7.2.4-1) unstable; urgency=medium

  * New upstream.
  * patches/build-nostrip.patch: Remove.  Applied upstream.
  * Refreshed other patches.

 -- Steve M. Robbins <smr@debian.org>  Fri, 24 Jun 2016 21:57:11 -0500

ipe (7.1.10-2) unstable; urgency=medium

  * patches/fix-moc.patch: Remove.  Use QT_SELECT to tell qtchooser which
    version of Qt we want.

  * control: Revert back to libjpeg-dev.  Appreciate the gentle prodding
    by all.  Closes: #805852, #814873.

 -- Steve M. Robbins <smr@debian.org>  Tue, 03 May 2016 23:04:20 -0500

ipe (7.1.10-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Change libpng12-dev build-dependency to libpng-dev, to ease libpng
    transition. (Closes: #810188)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 22 Jan 2016 11:09:34 +0100

ipe (7.1.10-1) unstable; urgency=medium

  * New upstream.
  * control: adjust build-dependencies.  Now builds with Qt5.
  * patches/fPIC.patch: Remove.  Applied upstream.
  * patches/default-browser.patch: New.  Configure to use sensible-browser.
  * patches/fix-moc.patch: New.  Temporary workaround for #805726.
  * patches/default-editor.patch: Fix error in patch.  Closes: #728480.

 -- Steve M. Robbins <smr@debian.org>  Sat, 21 Nov 2015 10:15:22 -0600

ipe (7.1.8-1) unstable; urgency=medium

  * New upstream.

 -- Steve M. Robbins <smr@debian.org>  Sun, 13 Sep 2015 09:27:23 -0500

ipe (7.1.4-2) unstable; urgency=low

  * copyright: Updated with patch from Felix Geyer.  Closes: #729740.
  * patches/default-editor.patch: New.  Change hardcoded default editor
    (when EDITOR not defined) to "xterminal-emulator -e editor", as
    suggested by Francesco Poli.  Closes: #728480.

 -- Steve M. Robbins <smr@debian.org>  Sat, 30 Nov 2013 22:28:30 -0600

ipe (7.1.4-1) unstable; urgency=low

  * New upstream.  Closes: #703269.

 -- Steve M. Robbins <smr@debian.org>  Mon, 04 Nov 2013 23:10:27 -0600

ipe (7.1.3-1) unstable; urgency=low

  * New upstream.  Update to standards-version 3.9.4, debhelper compat
    level 9.

 -- Steve M. Robbins <smr@debian.org>  Mon, 21 Jan 2013 00:11:36 -0600

ipe (7.1.2-1) unstable; urgency=low

  * New upstream.
  
  * ipe.preinst: New.  Remove old directory /usr/share/doc/ipe/doc if it
    exists on upgrade.  That path is now a symlink, but linking fails if
    the directory is there.  Closes: #654922.

 -- Steve M. Robbins <smr@debian.org>  Sun, 15 Jan 2012 22:33:06 -0600

ipe (7.1.1-1) unstable; urgency=low

  * New upstream.
    - Re-enabled compilation of omitted image ipelet.  Closes: #651594
  
  * debian/patches/fPIC.patch: New.  Build using -fPIC instead of -fpic.
    Closes: #641082.

 -- Steve M. Robbins <smr@debian.org>  Sat, 10 Dec 2011 16:55:09 -0600

ipe (7.1.0-1) unstable; urgency=low

  * New upstream version.

 -- Steve M. Robbins <smr@debian.org>  Tue, 30 Aug 2011 00:20:55 -0500

ipe (7.0.14-1) unstable; urgency=low

  * New upstream.  Update control, rules.
  
  * libipe7.0.13.install:
  * ipe.links: Remove.  Replace with explicit calls in rules using
    $(IPEVERS).  Fixes lua link.  Closes: #611560.

 -- Steve M. Robbins <smr@debian.org>  Tue, 15 Feb 2011 23:06:48 -0600

ipe (7.0.13-1) UNRELEASED; urgency=low

  * New upstream.

  * Move to debian-science SVN repository.  Closes: #584408.

 -- Steve M. Robbins <smr@debian.org>  Mon, 03 Jan 2011 22:23:16 -0600

ipe (7.0.10-2) unstable; urgency=low

  * control(ipe): Change dependency on lua interpreter (lua5.1) from
    depends to recommends.  Closes: #561721.
  
  * patches/build-nostrip.patch: New.  Install in staging directory
    without stripping, so that DEB_BUILD_OPTION 'nostrip' can be honoured.
    Closes: #437230.
  
  * rules: Install fontmap.xml correctly. Install ipe.so symlink in lua's
    package.cpath so that lua scripts can find ipe.  Closes: #562785.

 -- Steve M. Robbins <smr@debian.org>  Sun, 27 Dec 2009 18:11:32 -0600

ipe (7.0.10-1) unstable; urgency=low

  * New upstream.  Closes: #551192.
    - New build-depends: libcairo2-dev, liblua5.1-0-dev, gsfonts
    - patches/config.diff: Remove.  Upstream build system replaced.
    - Runtime lib package changed to libipe7.0.10 from libipe1c2a
    - Devel package renamed to libipe-dev (from libipe1-dev)
    - Package ipe depends on lua5.1 due to ipe-update-master.
  
  * rules: Re-write to use dh.

 -- Steve M. Robbins <smr@debian.org>  Fri, 11 Dec 2009 21:22:35 -0600

ipe (6.0pre32patch1-1) unstable; urgency=low

  * New upstream.
    - debian/patches/fix-version.diff: Remove.  Applied upstream.
    - debian/control: Remove conflicts with libfreetype6; upstream has
      removed run-time checks.

 -- Steve M. Robbins <smr@debian.org>  Sat, 09 May 2009 12:15:53 -0500

ipe (6.0pre32-2) unstable; urgency=low

  * debian/control: Update to build with freetype 2.3.9.  Update
    Standards-Version to 3.8.1.0; no changes required.

 -- Steve M. Robbins <smr@debian.org>  Sun, 15 Mar 2009 00:09:40 -0500

ipe (6.0pre32-1) unstable; urgency=low

  * New upstream.
    - debian/patches/fix-version.diff: New.  Set IPEVERS correctly.
    - debian/patches/update-webpages.diff: Remove.  Applied upstream.

 -- Steve M. Robbins <smr@debian.org>  Sun, 30 Nov 2008 22:10:26 -0600

ipe (6.0pre31-1) unstable; urgency=low

  * New upstream.
    - debian/patches/gcc43.diff
    - debian/patches/ipe-bug-253.diff:
    - debian/patches/decimal-point.patch: Remove.  Applied upstream.
    - debian/patches/config.diff: Remove IPEBROWSER hunk; upstream changed
      it to sensible-browser.

 -- Steve M. Robbins <smr@debian.org>  Fri, 07 Nov 2008 04:25:40 -0600

ipe (6.0pre30-6) unstable; urgency=low

  * debian/rules: Support parallel builds.  Grep src/config.pri for
    IPEVERS, rather than including it.  Closes: #482748.  Ensure source
    cleaned before unapplying quilt patches.
  
  * Convert existing sources changes to quilt series; new patches:
    - debian/patches/update-webpages.diff
    - debian/patches/config.diff
    - debian/patches/ipe-bug-253.diff
    - debian/patches/gcc43.diff

 -- Steve M. Robbins <smr@debian.org>  Fri, 07 Nov 2008 03:35:01 -0600

ipe (6.0pre30-5) unstable; urgency=low

  * debian/control: Adjust dependencies for freetype 2.3.7.  Replace
    recommends of "latex-ucs" by "texlive-latex-recommended".

 -- Steve M. Robbins <smr@debian.org>  Fri, 04 Jul 2008 21:59:45 -0500

ipe (6.0pre30-4) unstable; urgency=low

  * debian/control: Adjust dependencies for new freetype version.  Closes:
    #487041.  Remove (now obsolete) tetex-bin alternative from ipe
    dependencies.  Update to standards version 3.8.0.0.
  
  * debian/ipe.doc-base: Change section from Apps/Graphics to Graphics.

 -- Steve M. Robbins <smr@debian.org>  Thu, 19 Jun 2008 10:01:56 -0500

ipe (6.0pre30-3) unstable; urgency=low

  * debian/control:
  * debian/rules: Build using quilt.

  * debian/patches/decimal-point.patch: Set the LC_NUMERIC to "C" at
    program startup, to ensure decimal point is a period, not a comma.
    Closes: #480659.

 -- Steve M. Robbins <smr@debian.org>  Sun, 18 May 2008 23:00:04 -0500

ipe (6.0pre30-2) unstable; urgency=low

  * debian/control: Merge repeated Conflicts line for libipe1c2a.  Closes:
    #464303.  Add Homepage field; remove homepage from ipe description.
    Add sharutils build-dep.  Upgrade to standards-version 3.7.3; no
    changes required.  Switch from substvar Source-Version to
    binary:Version.

  * debian/rules: Install desktop file and icon.  Closes: #451022.  Note
    that I'm not applying the stylesheet part of the patch.
  
  * src/ipelib/ipedct.cpp:
  * src/ipelib/ipepdfwriter.cpp: Include <cstring>, to build with GCC 4.3
    (thanks brian m. carlson).  Closes: #454844.
  
  * src/ipecanvas/ipecanvaspainter.cpp:
  * src/ipecanvas/ipecanvaspainter.h:
  * src/ipecanvas/ipefonts.cpp:
  * src/ipelib/ipepdfparser.cpp: Applied patch dated 2007-12-14 for Ipe
    bug 253 (see http://http.theano.de/bugzilla).  Closes: #455247.

  * ipe/menu.ipe: Change from section Apps/ to section Applications/ per
    menu documentation.
  
 -- Steve M. Robbins <smr@debian.org>  Thu, 07 Feb 2008 00:49:24 -0600

ipe (6.0pre30-1) unstable; urgency=low

  * New upstream version.
    - Ipe now works with pdftex 1.40 (in MikTeX 2.6 and texlive 2007).
      Closes: #432043.

  * debian/control: Update download URL.  Closes: #433309.

 -- Steve M. Robbins <smr@debian.org>  Sun, 09 Dec 2007 07:24:19 -0600

ipe (6.0pre28-4) unstable; urgency=low

  * debian/control: Update depends and conflicts to new freetype (version
    2.3.5).  Closes: #432476.
  
  * debian/control: Add (<< 2007) to dependency on texlive-latex-base,
    since ipe can't cope with the output of version 1.40 of Pdflatex
    (included in in TexLive 2007).  Closes: #432043.
  
  * Update homepage URL.  Closes: #427388.
  
  * src/config.pri: Remove -Werror as upstream suggests
    (http://s105.theano.de/cgi-bin/bugzilla/show_bug.cgi?id=227).  Closes:
    #417675.

 -- Steve M. Robbins <smr@debian.org>  Tue, 10 Jul 2007 23:23:09 -0500

ipe (6.0pre28-3) unstable; urgency=low

  * Apply patch http://tclab.kaist.ac.kr/ipe/pdftex1.40patches.zip
  to enable Ipe to parse output of new pdftex 1.40.  See
  http://mail.cs.uu.nl/pipermail/ipe-discuss/2007-February/000622.html

 -- Steve M. Robbins <smr@debian.org>  Thu, 19 Apr 2007 20:26:00 -0500

ipe (6.0pre28-2) unstable; urgency=low

  * Introduce wrapper script around ipetoipe that uses a unique
    IPELATEXDIR for each run.  Closes: #405294.

 -- Steve M. Robbins <smr@debian.org>  Sun, 18 Mar 2007 19:13:13 -0500

ipe (6.0pre28-1) unstable; urgency=low

  * New upstream version.
  
  * tetex-fontmap.xml: Revert patches, since now using debian/gsfonts-fontmap.xml.

  * src/ipetopng/ipetopng.cpp: Revert patch.  The printf now uses IpeDocument::pages(),
    which returns int.

  * src/ipemodel/ipeprefs.cpp: Revert patch.  Upstream now targets Qt 4.2.

 -- Steve M. Robbins <smr@debian.org>  Mon,  5 Mar 2007 23:24:01 -0600

ipe (6.0pre27-3) unstable; urgency=low

  * debian/gsfonts-fontmap.xml: New.  Fontmap for fonts from gsfonts package.
  * debian/rules: Use gsfonts-fontmap.xml instead of tetex-fontmap.xml.
  * debian/control: Add texlive-latex-base dependency as alternative to
    tetex-bin (for pdflatex) and replace tetex-extra by gsfonts (for font
    files).  Patch courtesy of Norbert Preining.  Closes: #378537.

 -- Steve M. Robbins <smr@debian.org>  Tue,  9 Jan 2007 23:14:51 -0600

ipe (6.0pre27-2) unstable; urgency=low

  * src/ipetoipe/ipetoipe.cpp: Revert patch from 6.0pre26-3, since IpeDocument::TotalViews()
  now returns int, not size_t.  Closes: #395987.

 -- Steve M. Robbins <smr@debian.org>  Sun, 29 Oct 2006 07:44:45 -0600

ipe (6.0pre27-1) unstable; urgency=low

  * New upstream version.
  
  * src/ipemodel.pro: Revert previous change.  Use patch from Ipe Bug 176,
  provided by Laurent Rineau.  Closes: #395311.

 -- Steve M. Robbins <smr@debian.org>  Sat, 28 Oct 2006 19:35:10 -0500

ipe (6.0pre26-6) unstable; urgency=low
  
  * src/literal_escaped_quote.pri:
  * src/ipemodel.pro: Qt 4.2 changed the behaviour of backslash inside qmake
  file; this code should work on both 4.2 and previous versions.

 -- Steve M. Robbins <smr@debian.org>  Sun, 22 Oct 2006 23:23:41 -0500

ipe (6.0pre26-5) unstable; urgency=low

  * Rebuild with freetype 2.2.1.  Closes: #369244.  
  Both source and library package now conflict with freetype >= 2.2.2.

 -- Steve M. Robbins <smr@debian.org>  Sat,  1 Jul 2006 13:13:38 -0400

ipe (6.0pre26-4) unstable; urgency=low

  * No change.  Just want the autobuilders to pick this up.

 -- Steve M. Robbins <smr@debian.org>  Sat, 22 Apr 2006 08:40:54 -0400

ipe (6.0pre26-3) unstable; urgency=low

  * src/ipetoipe/ipetoipe.cpp:
  * src/ipetopng/ipetopng.cpp: Use %zu to print values of size_t.
    Closes: #361773.

 -- Steve M. Robbins <smr@debian.org>  Mon, 10 Apr 2006 23:47:37 -0400

ipe (6.0pre26-2) unstable; urgency=low

  * src/ipetoipe/ipetoipe.cpp: cast doc->size() to int for use in 
  fprintf( %d ).

 -- Steve M. Robbins <smr@debian.org>  Mon, 10 Apr 2006 00:09:13 -0400

ipe (6.0pre26-1) unstable; urgency=low

  * New upstream version.  Closes: #340558.
    - requires Qt4; change Build-Depends and debian/rules appropriately
    - build-conflict with libipe1-dev to avoid problems linking wrong lib
    - static libs are no longer built

 -- Steve M. Robbins <smr@debian.org>  Sun,  9 Apr 2006 20:19:07 -0400

ipe (6.0pre23-7) unstable; urgency=low

  * tetex-fontmap.xml: update font path to /usr/share/texmf-tetex/
  Closes: #355563.

 -- Steve M. Robbins <smr@debian.org>  Tue,  7 Mar 2006 22:38:54 -0500

ipe (6.0pre23-6) unstable; urgency=low

  * Remove (broken) attempt at tighter libfreetype dependency.

 -- Steve M. Robbins <smr@debian.org>  Sun, 12 Feb 2006 23:22:01 -0500

ipe (6.0pre23-5) unstable; urgency=low

  * Suggest package latex-ucs for unicode character support.  
    Closes: #345938.
  
  * Tighten dependency on libfreetype.  Closes: #345937.

 -- Steve M. Robbins <smr@debian.org>  Thu,  9 Feb 2006 00:02:18 -0500

ipe (6.0pre23-4) unstable; urgency=low

  * debian/control: Rename library packages to use "c2a" suffix, due to
    libstdc++ allocator change.  Declare conflicts & replaces with the
    previous "c2" packages.  Closes: #339188.

 -- Steve M. Robbins <smr@debian.org>  Wed, 23 Nov 2005 21:37:07 -0500

ipe (6.0pre23-3) unstable; urgency=low
  
  * GCC 4 transition: rename libipe1 to libipe1c2.

  * src/config.pro: Remove -Werror.  I'd prefer to add the required
  virtual destructors, but at least two problem classes are in Qt itself.
  Closes: #302606.
  
  * Rebuild with current freetype.  Closes: #315823.

 -- Steve M. Robbins <smr@debian.org>  Mon,  8 Aug 2005 21:02:26 -0400

ipe (6.0pre23-2) unstable; urgency=low

  * debian/control: Ipe should depend on exact version of libipe.
    Closes: #296771.

 -- Steve M. Robbins <smr@debian.org>  Thu, 24 Feb 2005 22:09:16 -0500

ipe (6.0pre23-1) unstable; urgency=low

  * New upstream version.

 -- Steve M. Robbins <smr@debian.org>  Sat, 18 Dec 2004 20:56:33 -0500

ipe (6.0pre22-2) unstable; urgency=low

  * src/config.pro: Set IPEDOCDIR to location where manual gets installed
    on Debian.  Closes: #277306.

 -- Steve M. Robbins <smr@debian.org>  Sun, 24 Oct 2004 11:16:35 -0400

ipe (6.0pre22-1) unstable; urgency=low

  * New upstream version.
  * New package libipe1-dev.

 -- Steve M. Robbins <smr@debian.org>  Mon, 11 Oct 2004 23:55:49 -0400

ipe (6.0pre20-2) unstable; urgency=low

  * Apply patch from upstream to address build failures.

 -- Steve M. Robbins <smr@debian.org>  Sun, 18 Jul 2004 16:15:34 -0400

ipe (6.0pre20-1) unstable; urgency=low

  * New upstream release.
    - fixes crash when creating circle through three points.  Closes: #259533.
    - debian/copyright: Update to current license terms.
    - debian/*.1: Remove as manpages now come from upstream.
  
  * Rename debian/menu to debian/ipe.menu.  Closes: #259539.

 -- Steve M. Robbins <smr@debian.org>  Tue, 29 Jun 2004 01:40:02 -0400

ipe (6.0pre16-2) unstable; urgency=low

  * src/ipemodel/ipepdfdoc.cpp: Duplicate string before putting it
  into the environment.  Applied patch from Hein Roehrig.  Closes: #253200.
  
  * debian/copyright: Update to current license terms.

 -- Steve M. Robbins <smr@debian.org>  Tue,  8 Jun 2004 00:44:02 -0400

ipe (6.0pre16-1) unstable; urgency=low

  * New upstream release.
  * New maintainer.  Closes: #237582.
  * Build against latest freetype.  Closes: #214983.

 -- Steve M. Robbins <smr@debian.org>  Sat,  5 Jun 2004 13:03:09 -0400

ipe (6.0pre10-3) unstable; urgency=low

  * Upgraded to policy 3.6.1.0
  * Improved description of libipe
    (closes: #209971)

 -- Pascal Hakim <pasc@debian.org>  Wed, 10 Sep 2003 11:53:23 +1000

ipe (6.0pre10-2) unstable; urgency=low

  * Forgot to change the version number in the build script
    (closes: #202117)

 -- Pascal Hakim <pasc@debian.org>  Sun, 20 Jul 2003 11:00:32 +1000

ipe (6.0pre10-1) unstable; urgency=low

  * New upstream release
    (closes: #201445)
  * Deleting all vertices in a polyline no longer dumps core.
    (Fixed in latest upstream release)
    (closes: #199539)
  * Fixed the description for libipe
    (closes: #194734)

 -- Pascal Hakim <pasc@debian.org>  Fri, 18 Jul 2003 22:38:08 +1000

ipe (6.0pre9-2) unstable; urgency=low

  * Fixed documentation section 
    (closes: #194490)

 -- Pascal Hakim <pasc@debian.org>  Sat, 24 May 2003 19:51:32 +1000

ipe (6.0pre9-1) unstable; urgency=low

  * Initial Release.

 -- Pascal Hakim <pasc@debian.org>  Mon, 31 Mar 2003 23:58:33 +1000

